<?php

namespace App\Admin;

use App\Entity\PostCategory;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class PostAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('title', TextType::class);
        $formMapper->add('slug', TextType::class);
        $formMapper->add('description', TextType::class);
        $formMapper->add('content', TextType::class);
        $formMapper->add('status', ChoiceType::class, [
            'choices' => [
                'Active' => 'active',
                'Disabled' => 'disabled',
                'Deleted' => 'deleted',
            ],
        ]);
        $formMapper->add('category', ModelType::class, [
            'class' => PostCategory::class,
            'property' => 'title',
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
        $datagridMapper->add('slug');
//        $datagridMapper->add('description');
//        $datagridMapper->add('content');
        $datagridMapper->add('status');
//        $datagridMapper->add('category');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
        $listMapper->addIdentifier('slug');
//        $listMapper->addIdentifier('description');
//        $listMapper->addIdentifier('content');
        $listMapper->addIdentifier('status');
//        $listMapper->addIdentifier('category');
        $listMapper->add('category', null, [
            'associated_property' => 'title'
        ]);
    }
}
