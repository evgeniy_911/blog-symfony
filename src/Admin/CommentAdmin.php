<?php

namespace App\Admin;

use App\Entity\Post;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class CommentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('comment', TextType::class);
        $formMapper->add('post', ModelType::class, [
            'class' => Post::class,
            'property' => 'title',
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('comment');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('comment');
        $listMapper->add('post', null, [
            'associated_property' => 'title'
        ]);
    }
}
