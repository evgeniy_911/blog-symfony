<?php

declare(strict_types = 1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class PostCategoryRepository extends EntityRepository
{
    public function getStatistics(): array
    {
        $connection = $this->getEntityManager()->getConnection();

        $stmt = $connection->prepare('
SELECT pc.id, pc.title, pc.slug, COUNT(p.id) AS post_count, p.`status` FROM post_category AS pc
JOIN post AS p ON p.category_id = pc.id
GROUP BY pc.slug
HAVING p.status = "active"
        ');
        $stmt->execute();

        $result = $stmt->fetchAll(); // fetchAll()

        return $result;
    }


//
//
//    public function getCountCategory()
//    {
//        $connection = $this->getEntityManager()->getConnection();
//
//        $stmt = $connection->prepare('SELECT COUNT(id) FROM post_category');
//        $stmt->execute();
//
//        $result = $stmt->fetch(); // fetchAll()
//
//        return $result['COUNT(id)'];
//    }
//
//    public function getMinCategoryId()
//    {
//        $connection = $this->getEntityManager()->getConnection();
//
//        $stmt = $connection->prepare('SELECT MIN(id) FROM post_category');
//        $stmt->execute();
//
//        $result = $stmt->fetch(); // fetchAll()
//
//        return $result['MIN(id)'];
//    }
//
//    public function getMaxCategoryId()
//    {
//        $connection = $this->getEntityManager()->getConnection();
//
//        $stmt = $connection->prepare('SELECT MAX(id) FROM post_category');
//        $stmt->execute();
//
//        $result = $stmt->fetch(); // fetchAll()
//
//        return $result['MAX(id)'];
//    }
}