<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Post;
use App\Entity\PostCategory;
use App\Repository\PostCategoryRepository;
use App\Repository\PostRepository;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
//    /**
//     * @Route("/admin")
//     */
//    public function admin(): Response
//    {
//
////        for ($i = 1; $i <= 5; $i++) {
////            $entityManager = $this->getDoctrine()->getManager();
////
////            $faker = Factory::create();
////
////            $product = new PostCategory();
////            $product->setTitle($faker->currencyCode);
////            $product->setSlug($faker->languageCode);
////
////            $entityManager->persist($product);
////
////            $entityManager->flush();
////        }
//
//        return new Response('added new record');
//    }

    /**
     * @Route("/")
     */
    public function index(): Response
    {
        $posts = $this->getPostRepository()->findBy(['status' => 'active']); // SELECT * FROM post WHERE status = 'active'
        $activeCategories = $this->getPostCategoryRepository()->getStatistics();

        return $this->render('home/index.html.twig', [
            'posts'      => $posts,
            'activeCategories' => $activeCategories,
        ]);
    }

    /**
     * @Route("/blog/{slug}")
     */
    public function view(Request $request): Response
    {
        $post = $this->getPostRepository()->findOneBy(['slug' => $request->get('slug')]);
        $post->setVisits($post->getVisits() + 1);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($post);
        $entityManager->flush();

        $activeCategories = $this->getPostCategoryRepository()->getStatistics();

        return $this->render('home/view.html.twig', [
            'post'       => $post,
            'activeCategories' => $activeCategories,
        ]);
    }

    /**
     * @Route("/category/{slug}")
     */
    public function category(Request $request): Response
    {
        $category = $this->getPostCategoryRepository()->findOneBy(['slug' => $request->get('slug')]);
        $activeCategories = $this->getPostCategoryRepository()->getStatistics();

        return $this->render('home/category.html.twig', [
            'category'   => $category,
            'activeCategories' => $activeCategories,
        ]);
    }

    /**
     * @Route("/contact-us", name="contact-us")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function contactUs(Request $request): Response
    {
        $activeCategories = $this->getPostCategoryRepository()->getStatistics();

        return $this->render('home/contact-us.html.twig', [
            'activeCategories' => $activeCategories,
        ]);
    }

    /**
     * @Route("/faq")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function faq(Request $request): Response
    {
        $activeCategories = $this->getPostCategoryRepository()->getStatistics();

        return $this->render('home/faq.html.twig', [
            'activeCategories' => $activeCategories,
        ]);
    }

    private function getPostCategoryRepository(): PostCategoryRepository
    {
        return $this->getDoctrine()->getRepository(PostCategory::class);
    }

    private function getPostRepository(): PostRepository
    {
        return $this->getDoctrine()->getRepository(Post::class);
    }
}

















//        $posts = $this->getDoctrine()->getRepository(Post::class)->findBy(['category' => 2]);